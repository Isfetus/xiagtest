//
//  AppDelegate.swift
//  XiagTest
//
//  Created by Andrey Belkin on 18/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        URLCacheConfigurator.configure()
        
        return true
    }
}

