//
//  ImagesService.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class ImagesService
{
    private let network:Networking
    
    init(network:Networking)
    {
        self.network = network
    }
    
    public func getImages(parameters: RequestParameters.Type, callback:@escaping (Bool, [PlainImageData]) -> ())
    {
        self.makeNetworkRequest(parameters: parameters, callback: callback)
    }
    
    func makeNetworkRequest(parameters: RequestParameters.Type, callback:@escaping (Bool, [PlainImageData]) -> ())
    {
        self.network.requestJSON(url: parameters.url,
                                 method: parameters.method,
                                 parameters: parameters.parameters,
                                 headers: parameters.headers,
                                 callBack: { (success: Bool, data: AnyObject?) -> () in
                                    
                                    if success
                                    {
                                        if data != nil
                                        {
                                            let plainData: [PlainImageData] = ImagesGalleryResponseMapper.parse(responseJSON: data!)
                                            
                                            callback(success, plainData)
                                        }
                                    }
        })
    }
}
