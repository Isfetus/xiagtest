//
//  ImagesGalleryInteractor.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

protocol ImagesGalleryInteractorIn
{
    func getImages(parameters: RequestParameters.Type, callback:@escaping (Bool, [PlainImageData]) -> ())
}

class ImagesGalleryInteractor: ImagesGalleryInteractorIn
{
    private let service:ImagesService
    
    init(service:ImagesService)
    {
        self.service = service
    }
    
    public func getImages(parameters: RequestParameters.Type, callback:@escaping (Bool, [PlainImageData]) -> ())
    {
        self.service.getImages(parameters: parameters, callback: callback)
    }
}
