//
//  ImagesGalleryPresenter.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

protocol ImagesGalleryPresenterIn
{
    func onViewWillAppear()
    func searchTextChange(text: String)
    func selectImage(indexPath: IndexPath)
    func shareButtonClicked()
    func onSharingComplete()
}

protocol ImagesGalleryPresenterOut : class
{
    func updateData(data: [ImageCellModel])
    func showActivityIndicator(_ isShow: Bool)
    func updateSharedImagesCount(count: Int)
    func shareImages(images: [UIImage])
    func setSharingState(isSharing: Bool)
}

class ImagesGalleryPresenter
{
    let interactor:ImagesGalleryInteractorIn
    weak var view:ImagesGalleryPresenterOut?
    
    var models:[ImageCellModel]? = nil
    var selectedModels:[ImageCellModel] = [ImageCellModel]()
    
    var searchText:String = ""
    var sharing:Bool = false
    
    public init(view: ImagesGalleryPresenterOut, interactor: ImagesGalleryInteractorIn)
    {
        self.interactor = interactor
        self.view = view
    }
    
    func updateDataWithSearchText(text: String)
    {
        if self.models != nil
        {
            if !(text.isEmpty)
            {
                let filteredModels = self.models!.filter{ ($0.getName()?.contains(text))! }
                
                self.view?.updateData(data: filteredModels)
                
            }
            else
            {
                self.view?.updateData(data: self.models!)
            }
        }
    }
    
    public func parsePlainDataToModels(plainData: [PlainImageData]) -> [ImageCellModel]
    {
        let network:ImageNetworking = Network()
        
        return plainData.map({ (plainObject: PlainImageData) -> ImageCellModel in
            
            ImageCellModel(network: network, name: plainObject.name, imageUrl: plainObject.url, imageTnUrl: plainObject.url_tn)
        })
    }
}

extension ImagesGalleryPresenter: ImagesGalleryPresenterIn
{
    public func onViewWillAppear()
    {
        self.view?.showActivityIndicator(true)
        self.interactor.getImages(parameters: ImagesGalleryRequest.self) {[weak self] (result: Bool, data:[PlainImageData]) in
            
            DispatchQueue.main.async
                {
                    self?.view?.showActivityIndicator(false)
                    
                    let models = self?.parsePlainDataToModels(plainData: data)
                    self?.models = models
                    self?.view?.updateData(data: models!)
            }
        }
    }
    
    func searchTextChange(text: String)
    {
        self.searchText = text
        
        self.updateDataWithSearchText(text: text)
    }

    func selectImage(indexPath: IndexPath)
    {
        if models != nil
        {
            let imageModel = self.models![indexPath.row]
            
            if let findedModelIndex = self.selectedModels.index(where: { $0 === imageModel })
            {
                self.selectedModels.remove(at: findedModelIndex)
            }
            else
            {
                self.selectedModels.append(self.models![indexPath.row])
            }
            
            self.view?.updateSharedImagesCount(count: self.selectedModels.count)
        }
    }
    
    func onSharingComplete()
    {
        self.view?.updateSharedImagesCount(count: 0)
        selectedModels.removeAll(keepingCapacity: false)
        self.view?.setSharingState(isSharing: false)
        self.sharing = false
    }
    
    func shareButtonClicked()
    {
        if selectedModels.isEmpty
        {
            sharing = !sharing
            
            self.view?.setSharingState(isSharing: sharing)
            
            if sharing
            {
                self.view?.updateSharedImagesCount(count: 0)
            }
            
            return
        }
        else
        {
            if sharing
            {
                var imageArray = [UIImage]()
                for selectedModel in selectedModels
                {
                    if let thumbnail = selectedModel.getImageTn()
                    {
                        imageArray.append(thumbnail)
                    }
                }
                
                if !imageArray.isEmpty
                {
                    self.view?.shareImages(images: imageArray)
                }
            }
        }
    }
}
