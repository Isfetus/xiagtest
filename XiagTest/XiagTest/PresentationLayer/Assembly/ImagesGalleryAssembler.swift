//
//  ImagesGalleryAssembler.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class ImagesGalleryAssembler {

    static public func getImagesGalleryPresenter(view: ImagesGalleryController) -> ImagesGalleryPresenter
    {
        let network = Network()
        let service = ImagesService(network: network)
        let interactor = ImagesGalleryInteractor(service: service)
        
        return ImagesGalleryPresenter.init(view: view, interactor: interactor)
    }
}
