//
//  ImageCellModel.swift
//  XiagTest
//
//  Created by Andrey Belkin on 20/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class ImageCellModel
{
    private var name: String?
    private var imageUrl: String?
    private var imageTnUrl: String?
    
    private var tnImage: UIImage?
    
    private var network: ImageNetworking
    
    init(network: ImageNetworking, name: String?, imageUrl:String?, imageTnUrl:String?)
    {
        self.network = network
        
        self.name = name
        self.imageUrl = imageUrl
        self.imageTnUrl = imageTnUrl
    }
    
    func getName() -> String?
    {
        return self.name
    }
    
    func getImageUrl() -> String?
    {
        return self.imageUrl
    }
    
    func getImageTnUrl() -> String?
    {
        return self.imageTnUrl
    }
    
    public func getImage(callback: @escaping (UIImage?) -> ())
    {
        if self.imageUrl != nil
        {
            self.network.requestImage(url: self.imageUrl!, callBack: callback)
        }
        else
        {
            callback(nil)
        }
    }
    
    public func getImageTn() -> UIImage?
    {
        return self.tnImage
    }
    
    public func getImageTn(callback: @escaping (UIImage?) -> ())
    {
        if self.imageTnUrl != nil
        {
            self.network.requestImage(url: self.imageTnUrl!, callBack: { [weak self] (image: UIImage?) -> () in
            
                self?.tnImage = image
                
                callback(image)
            })
        }
        else
        {
            callback(nil)
        }
    }
    
    func setName(name: String?)
    {
        self.name = name
    }
    
    func setImageUrl(url: String?)
    {
        self.imageUrl = url
    }
    
    func setImageTnUrl(url: String?)
    {
        self.imageTnUrl = url
    }
}
