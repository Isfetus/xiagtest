//
//  ViewController.swift
//  XiagTest
//
//  Created by Andrey Belkin on 18/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let CELLS_PADDING:CGFloat = 10.0
fileprivate let SECTION_INSETS = UIEdgeInsets(top: CELLS_PADDING, left: CELLS_PADDING, bottom: CELLS_PADDING, right: CELLS_PADDING)
fileprivate let ROWS_COUNT: CGFloat = 3
fileprivate let SEARCH_BAR_BACKGROUND_COLOR = UIColor.clear

fileprivate let IMAGE_CELL_NIB = "ImagesGalleryCell"
fileprivate let IMAGE_CELL_REUSE_ID = "imagesGalleryCell"

class ImagesGalleryController: UIViewController
{
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var shareButton: UIBarButtonItem!

    fileprivate var isSharingState = false
    fileprivate let shareTextLabel = UILabel()
    
    var selectedImageIndexPath:IndexPath? = nil
    {
        didSet
        {
            var indexPaths = [IndexPath]()
            if let selectedImageIndexPath = selectedImageIndexPath
            {
                indexPaths.append(selectedImageIndexPath)
            }
            
            if let oldValue = oldValue
            {
                indexPaths.append(oldValue)
            }
    
            collectionView?.performBatchUpdates({
                
                self.collectionView?.reloadItems(at: indexPaths)
            }) { [weak self] completed in
    
                if let selectedImageIndexPath = self?.selectedImageIndexPath
                {
                    self?.scrollCollectionView(indexPath: selectedImageIndexPath)
                }
                else
                {
                    if oldValue != nil
                    {
                        self?.scrollCollectionView(indexPath: oldValue!)
                    }
                }
            }
        }
    }
    
    var presenter: ImagesGalleryPresenterIn? = nil
    var network: ImageNetworking = Network()
    
    var data: [ImageCellModel]? = nil
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        self.presenter = ImagesGalleryAssembler.getImagesGalleryPresenter(view: self)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.configurateCollectionView()
        self.searchBar.backgroundColor = SEARCH_BAR_BACKGROUND_COLOR
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.presenter?.onViewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        self.addKeyboardObservers(scrollView: self.collectionView)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
    }
    
    func configurateCollectionView()
    {
        self.collectionView.register(UINib.init(nibName: IMAGE_CELL_NIB, bundle: nil), forCellWithReuseIdentifier: IMAGE_CELL_REUSE_ID)
    }
    
    @IBAction func shareButtonClicked()
    {
        self.presenter?.shareButtonClicked()
    }
    
    func getModelByIndexPath(indexPath: IndexPath) -> ImageCellModel
    {
        return self.data![indexPath.row]
    }
    
    func scrollCollectionView(indexPath: IndexPath)
    {
        self.collectionView?.scrollToItem(
            at: indexPath,
            at: .centeredVertically,
            animated: true)
    }
}

// MARK: ImagesGalleryPresenterOut
extension ImagesGalleryController: ImagesGalleryPresenterOut
{
    func updateSharedImagesCount(count: Int)
    {
        if count == 1
        {
            shareTextLabel.text = "\(count) " + NSLocalizedString("image selected", comment: "")
        }
        else
        {
            shareTextLabel.text = "\(count) " + NSLocalizedString("images selected", comment: "")
        }
        
        shareTextLabel.sizeToFit()
    }
    
    func updateData(data: [ImageCellModel])
    {
        self.data = data
        self.collectionView.reloadData()
    }
    
    func showActivityIndicator(_ isShow: Bool)
    {
        DispatchQueue.main.async
        {
            if isShow
            {
                self.activityIndicator.startAnimating()
            }
            else
            {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    func shareImages(images: [UIImage])
    {
        let shareScreen = UIActivityViewController(activityItems: images, applicationActivities: nil)
         shareScreen.completionWithItemsHandler = { [weak self] _ in
            
            self?.presenter?.onSharingComplete()
         }
        
         let popoverPresentationController = shareScreen.popoverPresentationController
         popoverPresentationController?.barButtonItem = self.shareButton
         popoverPresentationController?.permittedArrowDirections = .any
         present(shareScreen, animated: true, completion: nil)
    }
    
    func setSharingState(isSharing: Bool)
    {
        if isSharing
        {
            let sharingDetailItem = UIBarButtonItem(customView: shareTextLabel)
            navigationItem.setRightBarButtonItems([shareButton,sharingDetailItem], animated: true)
            
        }
        else
        {
            navigationItem.setRightBarButtonItems([shareButton], animated: true)
        }
        
        if let _ = selectedImageIndexPath
        {
            selectedImageIndexPath = nil
        }
        
        self.collectionView.allowsMultipleSelection = isSharing
        self.isSharingState = isSharing
        self.collectionView?.selectItem(at: nil, animated: true, scrollPosition: UICollectionViewScrollPosition())
    }
}

// MARK: UISearchBarDelegate
extension ImagesGalleryController: UISearchBarDelegate
{
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.presenter!.searchTextChange(text: searchText)
    }
    
    public func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        self.presenter!.searchTextChange(text: text)
        
        return true
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        self.presenter!.searchTextChange(text: "")
    }
    
}

// MARK: UICollectionViewDataSource
extension ImagesGalleryController:UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return (data != nil) ? 1 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return (data != nil) ? data!.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: IMAGE_CELL_REUSE_ID, for: indexPath) as! ImagesGalleryCell
        cell.setData(model: getModelByIndexPath(indexPath: indexPath))
        
        if self.selectedImageIndexPath == indexPath
        {
            cell.setBigImageAsBackground()
        }
        
        return cell
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension ImagesGalleryController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath == self.selectedImageIndexPath
        {
            return self.calculateBigCellSize(indexPath: indexPath)
        }

        let paddings = SECTION_INSETS.left * (ROWS_COUNT + 1)
        let availableWidth = view.frame.width - paddings
        let size = availableWidth / ROWS_COUNT

        return CGSize(width: size, height: size)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return SECTION_INSETS
    }

    func calculateBigCellSize(indexPath: IndexPath) -> CGSize
    {
        var size = collectionView.bounds.size
        size.height -= topLayoutGuide.length
        size.height -= (SECTION_INSETS.top + SECTION_INSETS.right)
        size.width -= (SECTION_INSETS.left + SECTION_INSETS.right)
        
        if let thumbnail = getModelByIndexPath(indexPath: indexPath).getImageTn()
        {
            let imageSize = thumbnail.size
            var returnSize = size
            
            let aspectRatio = imageSize.width / imageSize.height
            
            returnSize.height = returnSize.width / aspectRatio
            
            if returnSize.height > size.height
            {
                returnSize.height = size.height
                returnSize.width = size.height * aspectRatio
            }
            
            return returnSize
        }
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return SECTION_INSETS.left
    }
}

// MARK: UICollectionViewDelegate
extension ImagesGalleryController: UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView,
                                 shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        guard !isSharingState
        else
        {
            return true
        }
        
        self.selectedImageIndexPath = (selectedImageIndexPath == indexPath) ? nil : indexPath
        
        return false 
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 didSelectItemAt indexPath: IndexPath) {
        guard isSharingState
        else
        {
            return
        }
        
        self.presenter!.selectImage(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 didDeselectItemAt indexPath: IndexPath) {
        
        guard isSharingState
        else
        {
            return
        }
        
        self.presenter!.selectImage(indexPath: indexPath)
    }
}

