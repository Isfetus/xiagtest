//
//  ImagesGalleryCell.swift
//  XiagTest
//
//  Created by Andrey Belkin on 20/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class ImagesGalleryCell: UICollectionViewCell
{
    
    @IBOutlet var imageView:UIImageView!
    @IBOutlet var label:UILabel!
    @IBOutlet var labelBackground:UIView!
    @IBOutlet var selectionView:UIView!
    @IBOutlet var activityIndicator:UIActivityIndicatorView!
    
    override var isSelected: Bool
    {
        didSet
        {
            selectionView.isHidden = !isSelected
        }
    }
    
    var data:ImageCellModel? = nil
    {
        didSet
        {
            self.configureCell()
        }
    }
    
    public func setData(model: ImageCellModel)
    {
        self.data = model
    }
    
    private func configureCell()
    {
        self.label.text = self.data?.getName()
        
        if self.data?.getName() == nil
        {
            self.labelBackground.isHidden = true
        }
        else
        {
            self.labelBackground.isHidden = false
        }
        
        let imagePath = self.data?.getImageTnUrl()
        self.activityIndicator.startAnimating()
        
        self.data?.getImageTn(callback: { [weak self] (image: UIImage?) -> () in
        
            if imagePath == self?.data?.getImageTnUrl()
            {
                DispatchQueue.main.async
                {
                    self?.imageView.image = image
                    self?.activityIndicator.stopAnimating()
                }
            }
        })
    }
    
    func setBigImageAsBackground()
    {
        let imagePath = self.data?.getImageUrl()
        self.activityIndicator.startAnimating()
        
        self.data?.getImage(callback: { [weak self] (image: UIImage?) -> () in
            
            if imagePath == self?.data?.getImageUrl()
            {
                DispatchQueue.main.async
                    {
                        self?.imageView.image = image
                        self?.activityIndicator.stopAnimating()
                }
            }
        })
    }
    
    override func prepareForReuse()
    {
        self.imageView.image = nil
        self.activityIndicator.stopAnimating()
    }
}
