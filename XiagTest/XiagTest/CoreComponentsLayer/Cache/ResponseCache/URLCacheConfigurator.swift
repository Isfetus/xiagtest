//
//  ResponseCacheConfigurator.swift
//  XiagTest
//
//  Created by Andrey Belkin on 20/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let IN_MEMORY_CACHE_MAX_SIZE = 4 * 1024 * 1024
fileprivate let ON_DISK_CACHE_MAX_SIZE = 20 * 1024 * 1024 // 20 MB

class URLCacheConfigurator
{
    static func configure()
    {
        let urlCache = URLCache.init(memoryCapacity: IN_MEMORY_CACHE_MAX_SIZE, diskCapacity: ON_DISK_CACHE_MAX_SIZE, diskPath: nil)
        
        URLCache.shared = urlCache
    }
}
