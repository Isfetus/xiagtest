//
//  RequestParameters.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

public protocol RequestParameters
{
    static var url: String {get}
    static var method: HTTPMethod {get}
    static var parameters: [String:Any]? {get set}
    static var headers: [String:String]? {get set}
}
    


