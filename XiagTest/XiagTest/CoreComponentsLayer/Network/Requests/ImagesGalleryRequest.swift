//
//  ImagesGalleryRequest.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class ImagesGalleryRequest:RequestParameters
{
    static var headers: [String : String]? = nil
    
    internal static var url:String = "http://www.xiag.ch/share/testtask/list.json"
    internal static var parameters: [String: Any]? =  nil
    internal static var method: HTTPMethod
    {
        return .get
    }
}
