//
//  ImagesGalleryRequestMapper.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class ImagesGalleryResponseMapper
{
    static public func parse(responseJSON:AnyObject) -> [PlainImageData]
    {
        let imagesDataArray = responseJSON as! [[String:Any]]
        
        return imagesDataArray.map { (imageData:[String:Any]) -> PlainImageData in
                
            return PlainImageData.plainFromJSON(json: imageData)
        }
    }
    
}
