//
//  HttpMethod.swift
//  XiagTest
//
//  Created by Andrey Belkin on 18/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

public enum HTTPMethod : String {
    
    case get = "GET"
    
    case post = "POST"
    
    case put = "PUT"
    
    case delete = "DELETE"
}
