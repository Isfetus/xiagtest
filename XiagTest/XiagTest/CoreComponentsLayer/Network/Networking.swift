//
//  Networking.swift
//  XiagTest
//
//  Created by Andrey Belkin on 18/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation
import UIKit

public protocol ImageNetworking
{
    func requestImage(url: String, callBack: @escaping (UIImage?) -> ())
}

public protocol Networking: ImageNetworking
{
    func requestJSON(url: String, method:HTTPMethod, parameters: [String : Any]?, headers:[String : String]?, callBack: @escaping (Bool,
        AnyObject?) -> ())
}

