//
//  Network.swift
//  XiagTest
//
//  Created by Andrey Belkin on 18/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let validAPIStatusCodes:CountableRange<Int> = 200..<300
fileprivate let defaultValidAPIStatus:Int = 200

class Network:Networking
{
    public func requestJSON(url: String, method:HTTPMethod, parameters: [String : Any]?, headers:[String : String]?, callBack: @escaping (Bool,
        AnyObject?) -> ())
    {
        let request:URLRequest? = self.makeRequest(url: url, method: method, parameters: parameters, headers: headers)
        
        if request != nil
        {
            let session = URLSession.shared
            session.dataTask(with: request!) { (data, response, error) in
                
                if error == nil
                {
                    if let data = data
                    {
                        do
                        {
                            let json = try JSONSerialization.jsonObject(with: data, options: [])
                            
                            if let response = response as? HTTPURLResponse , validAPIStatusCodes ~= response.statusCode
                            {
                                callBack(true, json as AnyObject?)
                            }
                            else
                            {
                                callBack(false, json as AnyObject?)
                            }
                        }
                        catch
                        {
                            print(error)
                        }
                    }
                }
                else
                {
                    print(error!.localizedDescription)
                }
                
            }.resume()
        }
    }
    
    public func requestImage(url: String, callBack: @escaping (UIImage?) -> ())
    {
        guard let url = URL(string:url) else { return }
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            guard   let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == defaultValidAPIStatus,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else
                    {
                        callBack(nil)
                        return
                    }
            
            callBack(image)
            
        }.resume()
    }
    
    private func makeGetRequest(url: String, parameters: [String : Any]?, headers:[String : String]?) -> URLRequest?
    {
        guard var urlComponents = URLComponents(string: url)
        else{ return nil }
        
        if parameters != nil
        {
            var items = [URLQueryItem]()
                
            for (key,value) in parameters! {
                items.append(URLQueryItem(name: key, value: "\(value)"))
            }
 
            urlComponents.queryItems = items
        }
        
        guard let url = urlComponents.url
        else{ return nil }
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = headers

        return request
    }    

    
    private func makeRequest(url: String, method:HTTPMethod, parameters: [String : Any]?, headers:[String : String]?) -> URLRequest?
    {
        if method == .get
        {
            return makeGetRequest(url: url, parameters: parameters, headers: headers)
        }
        else
        {
            guard let url = URL(string:url) else { return nil}
            
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.allHTTPHeaderFields = headers
            
            if parameters != nil
            {
                guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters!) else { return nil}
                request.httpBody = httpBody
            }
            
            return request
        }
    }    
}
