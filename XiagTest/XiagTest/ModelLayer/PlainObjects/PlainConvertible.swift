//
//  PlainConvertible.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation

protocol Decodable
{
    static func plainFromJSON(json:[String:Any]) -> Self
}
