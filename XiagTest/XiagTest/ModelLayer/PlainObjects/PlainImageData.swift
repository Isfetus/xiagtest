//
//  PlainImageData.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

struct PlainImageData: Decodable
{
    public let name: String?
    public let url: String?
    public let url_tn: String?
    
    public init(name: String?, url: String?, url_tn: String?)
    {
        self.name = name
        self.url = url
        self.url_tn = url_tn
    }
    
    public static func plainFromJSON(json:[String:Any]) -> PlainImageData
    {
        return PlainImageData(name: json["name"] as? String,
                              url: json["url"] as? String,
                              url_tn: json["url_tn"] as? String)
    
    }
}


