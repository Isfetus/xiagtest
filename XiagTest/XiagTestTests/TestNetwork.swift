//
//  TestNetwork.swift
//  XiagTest
//
//  Created by Andrey Belkin on 19/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest

fileprivate var firstParam = "a"
fileprivate var firstParamValue = 1
fileprivate var secondParam = "b"
fileprivate var secondParamValue = "2"
fileprivate var thirdParam = "c"
fileprivate var thirdParamValue:Double = 3.0

fileprivate let parameters:[String: Any] = [
    firstParam: firstParamValue,
    secondParam: secondParamValue,
    thirdParam: thirdParamValue
]

class TestNetwork: XCTestCase
{
    var network:Networking? = Network()
    

    
    override func setUp()
    {
        super.setUp()
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        self.network = nil
    }
    
    func testGetRequest()
    {
        let expect = expectation(description: "waiting for GET response")
        
        network!.requestJSON(url: "https://httpbin.org/get", method: .get, parameters: parameters, headers: nil, callBack:{ (success:Bool, data:AnyObject?) -> () in
            
            XCTAssertTrue(success)
            
            if let jsonResult = data as? [String: Any]
            {
                for (key,value) in parameters
                {
                    XCTAssertTrue(((jsonResult["args"] as! [String: Any])[key] as! String) == "\(value)")
                }
                
                expect.fulfill()
            }
        })
    
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPostRequest()
    {
        let expect = expectation(description: "waiting for POST response")
        
        network!.requestJSON(url: "https://httpbin.org/post", method: .post, parameters: parameters, headers: nil, callBack:{ (success:Bool, data:AnyObject?) -> () in
            
            XCTAssertTrue(success)
            
            if let jsonResult = data as? [String: Any]
            {
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[firstParam] as! Int == parameters[firstParam] as! Int)
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[secondParam] as! String == parameters[secondParam] as! String)
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[thirdParam] as! Double == parameters[thirdParam] as! Double)
                
                expect.fulfill()
            }
        })
        
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testDeleteRequest()
    {
        let expect = expectation(description: "waiting for DELETE response")
        
        network!.requestJSON(url: "https://httpbin.org/delete", method: .delete, parameters: parameters, headers: nil, callBack:{ (success:Bool, data:AnyObject?) -> () in
            
            XCTAssertTrue(success)
            
            if let jsonResult = data as? [String: Any]
            {
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[firstParam] as! Int == parameters[firstParam] as! Int)
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[secondParam] as! String == parameters[secondParam] as! String)
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[thirdParam] as! Double == parameters[thirdParam] as! Double)
                
                expect.fulfill()
            }
        })
        
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPutRequest()
    {
        let expect = expectation(description: "waiting for PUT response")
        
        network!.requestJSON(url: "https://httpbin.org/put", method: .put, parameters: parameters, headers: nil, callBack:{ (success:Bool, data:AnyObject?) -> () in
            
            XCTAssertTrue(success)
            
            if let jsonResult = data as? [String: Any]
            {
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[firstParam] as! Int == parameters[firstParam] as! Int)
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[secondParam] as! String == parameters[secondParam] as! String)
                XCTAssertTrue((jsonResult["json"] as! [String: Any])[thirdParam] as! Double == parameters[thirdParam] as! Double)
                
                expect.fulfill()
            }
        })
        
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testLoadImage()
    {
        let expect = expectation(description: "waiting for image loading")
        
        network!.requestImage(url: "https://httpbin.org/image/png") { (image:UIImage?) in
            
            XCTAssertNotNil(image)
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testLoadImageWithWrongPath()
    {
        let expect = expectation(description: "waiting for no image loading")
        
        network!.requestImage(url: "https://wrong.path/image/") { (image:UIImage?) in
            
            XCTAssertNil(image)
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
    }
}
